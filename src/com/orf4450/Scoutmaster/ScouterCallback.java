package com.orf4450.Scoutmaster;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 1/14/2016
 */
public interface ScouterCallback {
	void onDataRecieved(LinkedList<HashMap<String, HashMap<String, Object>>> data_list);
}
