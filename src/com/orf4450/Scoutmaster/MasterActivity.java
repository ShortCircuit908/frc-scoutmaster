package com.orf4450.Scoutmaster;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ToggleButton;

import java.io.IOException;

public class MasterActivity extends Activity {
	private IncomingConnectionSink sink;
	private ScouterDatabase database_helper;
	private BluetoothManager bluetooth_manager;
	private ArrayAdapter<RemoteDeviceWrapper> list_adapter;
	private ToggleButton switch_accept_connections;

	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		setContentView(R.layout.main);
		database_helper = new ScouterDatabase(this);
		bluetooth_manager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
		list_adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
		ListView list_active_connections = (ListView) findViewById(R.id.list_active_connections);
		list_active_connections.setAdapter(list_adapter);
		switch_accept_connections = (ToggleButton) findViewById(R.id.switch_accept_connections);
		switch_accept_connections.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (switch_accept_connections.isChecked()) {
					createSink();
				}
				else {
					closeSink();
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		closeSink();
	}

	private void closeSink() {
		if (sink != null) {
			try {
				sink.close();
				sink = null;
				new AlertDialog.Builder(this).setTitle("Sink destroyed")
						.setMessage("Device is no longer listening for connections")
						.setIcon(android.R.drawable.ic_delete)
						.show();
			}
			catch (IOException e) {
				if (!isFinishing()) {
					new AlertDialog.Builder(this).setTitle("Could not destroy sink")
							.setMessage(e.getClass().getName() + ": " + e.getMessage())
							.setIcon(android.R.drawable.ic_dialog_alert)
							.show();
				}
				else {
					e.printStackTrace();
				}
			}
		}
	}

	private void createSink() {
		if (sink != null) {
			new AlertDialog.Builder(this).setTitle("Could not create sink")
					.setMessage("Connection sink is already running")
					.setIcon(android.R.drawable.ic_dialog_alert)
					.show();
			return;
		}
		try {
			sink = new IncomingConnectionSink(bluetooth_manager, database_helper, list_adapter);
			new AlertDialog.Builder(this).setTitle("Sink created")
					.setMessage("Device is now listening for connections")
					.setIcon(android.R.drawable.stat_sys_data_bluetooth)
					.show();
		}
		catch (IOException e) {
			new AlertDialog.Builder(this).setTitle("Could not create sink")
					.setMessage(e.getClass().getName() + ": " + e.getMessage())
					.setIcon(android.R.drawable.ic_dialog_alert)
					.show();
		}
	}
}
