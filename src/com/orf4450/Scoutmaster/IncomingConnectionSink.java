package com.orf4450.Scoutmaster;

import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.widget.ArrayAdapter;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 1/14/2016
 */
public class IncomingConnectionSink implements Runnable {
	private final BluetoothManager bluetooth_manager;
	private final BluetoothServerSocket server_socket;
	private final ScouterDatabase database;
	private final ArrayAdapter<RemoteDeviceWrapper> list_adapter;

	public IncomingConnectionSink(BluetoothManager bluetooth_manager, ScouterDatabase database, ArrayAdapter<RemoteDeviceWrapper> list_adapter) throws IOException {
		this.bluetooth_manager = bluetooth_manager;
		this.database = database;
		this.list_adapter = list_adapter;
		this.server_socket = bluetooth_manager.getAdapter().listenUsingRfcommWithServiceRecord(
				"Scoutmaster", UUID.fromString("7674047e-6e47-4bf0-831f-209e3f9dd23f"));
		new Thread(this).start();
	}

	@Override
	public void run() {
		while (true) {
			try {
				BluetoothSocket socket = server_socket.accept();
				new ConnectedScouter(socket, new ScouterCallback() {
					@Override
					public void onDataRecieved(LinkedList<HashMap<String, HashMap<String, Object>>> data_list) {
						for (HashMap<String, HashMap<String, Object>> data : data_list) {
							database.saveMatch(data);
						}
					}
				}, list_adapter);
			}
			catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
	}

	public void close() throws IOException {
		server_socket.close();
	}
}
